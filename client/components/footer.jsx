import React from 'react';

export default Footer = () => (
  <footer className="page-footer grey">
    <div className="container">
      <div className="row">

        <div className="col l3 s12">
          <h5 className="white-text">משרות</h5>
          <ul>
            <li>
              <a className="white-text" href="#!">Link 1</a>
            </li>
            <li>
              <a className="white-text" href="#!">Link 2</a>
            </li>
            <li>
              <a className="white-text" href="#!">Link 3</a>
            </li>
            <li>
              <a className="white-text" href="#!">Link 4</a>
            </li>
          </ul>
        </div>
        <div className="col l3 s12">
          <h5 className="white-text">אירועים</h5>
          <ul>
            <li>
              <a className="white-text" href="#!">Link 1</a>
            </li>
            <li>
              <a className="white-text" href="#!">Link 2</a>
            </li>
            <li>
              <a className="white-text" href="#!">Link 3</a>
            </li>
            <li>
              <a className="white-text" href="#!">Link 4</a>
            </li>
          </ul>
        </div>
        <div className="col l6 s12">
          <h5  dir="rtl" className="white-text"> מי אנחנו?</h5>
          <p  dir="rtl" className="grey-text text-lighten-4">בינואר 2016 יצא לדרך תהליך יוצא דופן בעולם החברתי - מיזוג בין עמותת "קו הזינוק" לארגון "שגרירי רוטשילד".

            ארגון שגרירי רוטשילד הוקם וגדל בתוך קרן רוטשילד קיסריה, הבשיל, פרש כנפיים וחבר לעמותת קו הזינוק שמתמחה בתוכנית מנהיגות ארוכת טווח (עשר שנים).

            יחד הפכו לעמותה שגאה להכריז  על שמה  - שותפויות רוטשילד קיסריה.

            החיבור הוא אך טבעי , יוצר המשכיות כרונולוגית (15-30) וטומן בחובו  התמקצעות וקפיצת מדרגה ביכולת ההשפעה החברתית של שתי התכניות לחוד וגם יחד, תוכניות אשר להן חזון משותף לפיתוח מנהיגות, צברו יחד שנים רבות של ניסיון, ידע מקצועי נרחב, מגוון גדול של שותפים ועוד.

            המיזוג מדגים יכולת לייצר סינרגיה ברמת שיתוף הפעולה הגבוהה ביותר -  מיזוג - ובכך ייחודי  לחברה האזרחית  ומהווה חלק מתפיסת העמותה לגבי חיבורים עתידיים אפשריים.

            אנחנו בתחילתו של מסע התפתחות וגדילה, מסע של הזדמנויות לייצר משהו חדש, מסע שמייצר התרגשות רבה.

            שותפים רבים ונאמנים  איתנו לאורך הדרך והם  חלק בלתי נפרד מהכוחות שלנו. הגב והעוצמה שלהם מאפשרים לנו להיות חדורי אמונה, מכווני מטרה ולפעול בכל המרץ להגשמת ייעודנו.

            אנו מזמינים  גם אתכם להצטרף אלינו ולהיות חלק מהמסע....

            דפי בירן זינגר, מנכ"לית</p>
        </div>
      </div>
    </div>

  </footer>
);
